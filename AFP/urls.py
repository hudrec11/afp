"""AFP URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import patterns, include, url
from django.contrib import admin
from apps.core import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.HomePageView.as_view(), name='home'),
    url(r'^ahorros$', views.AhorrosPageView.as_view(), name='ahorros'),
    url(r'^rentabilidad$', views.RentabilidadPageView.as_view(), name='rentabilidad'),
    url(r'^resultados$', views.ResultadosPageView.as_view(), name='resultados'),
    url(r'^signup$', views.signupView, name='signup'),
    url(r'^login$', 'django.contrib.auth.views.login',
        {'template_name': 'core/login.html'}, name='login'),
    url(r'^logout$', 'django.contrib.auth.views.logout_then_login',
        name='logout'),
]
