# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20161125_0728'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aportacion',
            name='fondo',
            field=models.ForeignKey(null=True, blank=True, to='core.Fondo', unique=True),
        ),
    ]
