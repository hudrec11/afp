# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20161125_0602'),
    ]

    operations = [
        migrations.RenameField(
            model_name='aportacion',
            old_name='fecha_fin_aportacion',
            new_name='fecha_fin',
        ),
        migrations.RenameField(
            model_name='aportacion',
            old_name='fecha_inicio_aportacion',
            new_name='fecha_inicio',
        ),
    ]
