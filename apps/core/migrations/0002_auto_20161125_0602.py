# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Aportacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sueldo', models.DecimalField(max_digits=10, decimal_places=2)),
                ('fecha_inicio_aportacion', models.DateField(null=True, blank=True)),
                ('fecha_fin_aportacion', models.DateField(null=True, blank=True)),
                ('monto', models.DecimalField(max_digits=10, decimal_places=2)),
            ],
        ),
        migrations.CreateModel(
            name='Fondo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ahorro', models.DecimalField(max_digits=10, decimal_places=2)),
                ('tea', models.DecimalField(max_digits=10, decimal_places=2)),
                ('persona', models.ForeignKey(to='core.Persona', unique=True)),
                ('tipo_fondo', models.ForeignKey(related_name='tipo_fondo', blank=True, to='core.Catalogo', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='aportacion',
            name='fondo',
            field=models.ForeignKey(to='core.Fondo', unique=True),
        ),
    ]
