# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Catalogo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('numero_dato', models.CharField(max_length=5)),
                ('dato', models.CharField(max_length=100)),
                ('abreviatura', models.CharField(max_length=5, null=True, blank=True)),
                ('valor1', models.CharField(max_length=30, null=True, blank=True)),
                ('valor2', models.CharField(max_length=30, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dni', models.CharField(unique=True, max_length=15)),
                ('nombre', models.CharField(max_length=200)),
                ('apellido', models.CharField(max_length=200)),
                ('fecha_nacimiento', models.DateField(null=True, blank=True)),
                ('estado_civil', models.ForeignKey(related_name='estado_civil', blank=True, to='core.Catalogo', null=True)),
                ('sexo', models.ForeignKey(related_name='sexo', blank=True, to='core.Catalogo', null=True)),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='TipoCatalogo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=30)),
            ],
        ),
        migrations.AddField(
            model_name='catalogo',
            name='tipo_catalogo',
            field=models.ForeignKey(to='core.TipoCatalogo'),
        ),
    ]
