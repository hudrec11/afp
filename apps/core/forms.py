# -- coding: utf-8 --
from django import forms
from django.contrib.auth.models import User
import models
import constants


class RegistrationForm(forms.Form):
    email = forms.EmailField(max_length=50, )
    username = forms.CharField(max_length=30)
    password = forms.PasswordInput()
    # rest of the fields

    def save(self):
        cleaned_data = super(RegistrationForm, self).clean()
        user = User()
        user.email = cleaned_data.get("username")
        user.username = cleaned_data.get("username")
        user.save(False)
        user.set_password = self.data.get("password")
        user.save()

        return user
        # you can validate those here

    def clean_username(self):
        username = self.cleaned_data.get('username')
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError("El usuario ya existe.")


    class Meta:
        model = User


class PersonaForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PersonaForm, self).__init__(*args, **kwargs)
        self.fields['sexo'].queryset = models.Catalogo.objects.filter(tipo_catalogo=constants.ID_SEXO)
        self.fields['estado_civil'].queryset = models.Catalogo.objects.filter(tipo_catalogo=constants.ID_ESTADO_CIVIL)

        for field, campo in self.fields.iteritems():
            if field == 'fecha_nacimiento':
                campo.widget.attrs['class'] = 'datepicker form-control'
                campo.widget.attrs['maxlength'] = '10'
                campo.input_formats = ('%d/%m/%Y',)
            else:
                if field == 'dni':
                    campo.widget.attrs['class'] = 'numero doi form-control'
                else:
                    campo.widget.attrs['class'] = 'form-control'

    class Meta:
        model = models.Persona
        fields = '__all__'


class FondoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(FondoForm, self).__init__(*args, **kwargs)
        self.fields['tipo_fondo'].queryset = models.Catalogo.objects.filter(tipo_catalogo=constants.ID_TIPO_FONDO)

        for field, campo in self.fields.iteritems():

            if field in ['ahorro','tea']:
                campo.widget.attrs['class'] = 'numero form-control'
            else:
                campo.widget.attrs['class'] = 'form-control'

    class Meta:
        model = models.Fondo
        fields = '__all__'


class AportacionForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AportacionForm, self).__init__(*args, **kwargs)
        self.fields['monto'].widget.attrs['readonly'] = ""

        for field, campo in self.fields.iteritems():
            if field in ['fecha_inicio', 'fecha_fin']:
                campo.widget.attrs['class'] = 'datepicker form-control'
                campo.widget.attrs['maxlength'] = '10'
                campo.input_formats = ('%d/%m/%Y',)
            else:
                if field == 'monto':
                    campo.widget.attrs['class'] = 'numero form-control'
                else:
                    campo.widget.attrs['class'] = 'form-control'

    class Meta:
        model = models.Aportacion
        fields = '__all__'