from django.contrib import admin
from . import models
from . import forms


# Register your models here.


class CatalogoInline(admin.TabularInline):
    model = models.Catalogo


class TipoCatalogoAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    list_display = ("id", "nombre")
    inlines = [
        CatalogoInline,
    ]


class CatalogoAdmin(admin.ModelAdmin):
    search_fields = ['dato']
    list_display = ("id", "tipo_catalogo", "numero_dato", "dato", "abreviatura", "valor1", "valor2")


class PersonaAdmin(admin.ModelAdmin):
    search_fields = ['dni', 'nombre', 'apellido']
    list_display = ('dni', 'nombre', 'apellido')
    form = forms.PersonaForm


class AportacionInline(admin.TabularInline):
    model = models.Aportacion


class FondoAdmin(admin.ModelAdmin):
    search_fields = ['persona',]
    list_display = ('persona', )
    form = forms.FondoForm
    inlines = [
        AportacionInline,
    ]


admin.site.register(models.TipoCatalogo, TipoCatalogoAdmin)
admin.site.register(models.Catalogo, CatalogoAdmin)
admin.site.register(models.Persona, PersonaAdmin)
admin.site.register(models.Fondo, FondoAdmin)
