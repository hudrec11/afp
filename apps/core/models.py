from __future__ import unicode_literals

from django.db import models
from django.contrib.auth import models as auth_models
# Create your models here.


class TipoCatalogo(models.Model):

    nombre = models.CharField(max_length=30)

    def __unicode__(self):
        return self.nombre


class Catalogo(models.Model):

    tipo_catalogo = models.ForeignKey(TipoCatalogo)
    numero_dato = models.CharField(max_length=5)
    dato = models.CharField(max_length=100)
    abreviatura = models.CharField(max_length=5,null=True,blank=True)
    valor1 = models.CharField(max_length=30,null=True,blank=True)
    valor2 = models.CharField(max_length=30,null=True,blank=True)

    def __unicode__(self):
        return self.dato


class Persona(models.Model):

    dni = models.CharField(max_length=15, unique=True)
    nombre = models.CharField(max_length=200)
    apellido = models.CharField(max_length=200)
    estado_civil = models.ForeignKey(Catalogo, related_name="estado_civil",null=True,blank=True)
    fecha_nacimiento = models.DateField(null=True,blank=True)
    sexo = models.ForeignKey(Catalogo, related_name="sexo",null=True,blank=True)
    usuario = models.ForeignKey(auth_models.User, unique=True)

    def __unicode__(self):
        return self.nombre +" " + self.apellido


class Fondo(models.Model):
    persona = models.ForeignKey(Persona, unique=True)
    ahorro = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    tipo_fondo = models.ForeignKey(Catalogo, related_name="tipo_fondo",null=True,blank=True)
    tea = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)


class Aportacion(models.Model):
    fondo = models.ForeignKey(Fondo, unique=True, null=True, blank=True)
    sueldo = models.DecimalField(max_digits=10, decimal_places=2)
    fecha_inicio = models.DateField(null=True, blank=True)
    fecha_fin = models.DateField(null=True, blank=True)
    monto = models.DecimalField(max_digits=10, decimal_places=2)