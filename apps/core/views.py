import  json
# Create your views here.
from decimal import Decimal
from django.contrib.auth import authenticate, login
from django.shortcuts import redirect, render
from django import http

# Create your views here.
from django.conf import settings
from django.views.generic import TemplateView, View

from .forms import RegistrationForm, PersonaForm, FondoForm, AportacionForm
from .models import Persona, Fondo, Aportacion, Catalogo
import constants


class AuthenticatedView(TemplateView):
    """Class for a view with login required"""

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return redirect(settings.LOGIN_URL)

        return super(AuthenticatedView, self).dispatch(request,
                *args, **kwargs)


class JSONResponseMixin(object):
    """Class for a view thats only response in json format"""

    @classmethod
    def get_json_response(cls, content, **httpresponse_kwargs):
        """Construct a 'HttpResponse' object"""
        return http.HttpResponse(content, content_type='application/json',
                                 **httpresponse_kwargs)

    @classmethod
    def convert_context_to_json(cls, context):
        """Convert the context dictionary into a JSON object"""
        # Note: This is *EXTREMELY* naive; in reality, you'll need
        # to do much more complex handling to ensure that arbitrary
        # objects -- such as Django model instances or querysets
        # -- can be serialized as JSON.
        if context:
            return json.dumps(context)
        else:
            return ''

    def render_to_response(self, context):
        """Returns a JSON response containing 'context' as payload"""
        return self.get_json_response(self.convert_context_to_json(context))


class HomePageView(AuthenticatedView):
    template_name = 'core/datos.html'
    persona_form = None
    mensaje = ""

    def get(self, request, tipo=None, *args, **kwargs):

        if request.user.is_authenticated():
            personas = Persona.objects.filter(usuario=request.user)
            if personas:
                self.persona_form = PersonaForm(instance=personas.first())
            else:
                self.persona_form = PersonaForm()
                self.persona_form.initial['usuario'] = request.user.id

            if 'fecha_nacimiento' in self.persona_form.initial.keys():
                    self.persona_form.initial['fecha_nacimiento'] = self.persona_form.initial['fecha_nacimiento'].strftime('%d/%m/%Y')
        else:
            redirect('login')
        result = self.render_to_response(self.get_context_data())
        return result

    def post(self, request, tipo=None):
        personas = Persona.objects.filter(usuario=request.user)
        if personas:
            self.persona_form = PersonaForm(request.POST,instance=personas.first())
        else:
            self.persona_form = PersonaForm(request.POST)

        if self.persona_form.is_valid():
            self.persona_form.save()
            self.mensaje = "Se guardo satisfactoriamente"

        result = self.render_to_response(self.get_context_data())
        return result

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context['persona_form'] = self.persona_form
        context['mensaje'] = self.mensaje

        return context


def calcular_ahorro(aporte):
    tea = aporte.fondo.tea/100
    tem = ((1+tea) ** Decimal(30.0/360.0)) - 1
    n = (aporte.fecha_fin - aporte.fecha_inicio).days//30
    monto = aporte.monto
    ahorro_num = (1+tem) ** n - 1
    ahorro = monto*(ahorro_num/tem)
    return round(ahorro,2)


class AhorrosPageView(AuthenticatedView):
    template_name = 'core/ahorros.html'
    fondo_form = None
    aportacion_form = None
    mensaje = ""
    por_aportacion = Catalogo.objects.filter(tipo_catalogo=constants.ID_APORTACION)[0]

    def get(self, request, tipo=None, *args, **kwargs):

        if request.user.is_authenticated():
            personas = Persona.objects.filter(usuario=request.user)
            if personas:
                persona = personas.first()
            else:
                redirect('home')

            fondos = Fondo.objects.filter(persona=persona)
            if fondos:
                fondo = fondos.first()
                self.fondo_form = FondoForm(instance=fondo)
                aportaciones = Aportacion.objects.filter(fondo=fondo)
                if aportaciones:
                    aportacion = aportaciones.first()
                    self.aportacion_form = AportacionForm(instance=aportacion)
                else:
                    self.aportacion_form = AportacionForm()
            else:
                self.fondo_form = FondoForm()
                self.fondo_form.initial['ahorro'] = 0
                self.fondo_form.initial['persona'] = persona
                self.aportacion_form = AportacionForm()

            if 'fecha_inicio' in self.aportacion_form.initial.keys():
                self.aportacion_form.initial['fecha_inicio'] = self.aportacion_form.initial['fecha_inicio'].strftime('%d/%m/%Y')
            if 'fecha_fin' in self.aportacion_form.initial.keys():
                self.aportacion_form.initial['fecha_fin'] = self.aportacion_form.initial['fecha_fin'].strftime('%d/%m/%Y')

        else:
            redirect('login')

        result = self.render_to_response(self.get_context_data())
        return result

    def post(self, request, tipo=None):
        if request.user.is_authenticated():
            personas = Persona.objects.filter(usuario=request.user)
            if personas:
                persona = personas.first()
            else:
                redirect('home')

            fondos = Fondo.objects.filter(persona=persona)
            if fondos:
                fondo = fondos.first()
                self.fondo_form = FondoForm(request.POST, instance=fondo)
                aportaciones = Aportacion.objects.filter(fondo=fondo)
                if aportaciones:
                    aportacion = aportaciones.first()
                    self.aportacion_form = AportacionForm(request.POST, instance=aportacion)
                else:
                    self.aportacion_form = AportacionForm(request.POST,)
            else:
                self.fondo_form = FondoForm(request.POST,)
                self.aportacion_form = AportacionForm(request.POST,)

            if self.fondo_form.is_valid():
                fondo = self.fondo_form.save()
                if self.aportacion_form.is_valid():
                    aportacion = self.aportacion_form.save(False)
                    aportacion.fondo = fondo
                    aportacion.save()
                    fondo.ahorro = calcular_ahorro(aportacion)
                    fondo.save()
                    self.mensaje = "Se guardo satisfactoriamente"
                else:
                    self.mensaje = "No de pudo guardar intentelo mas tarde"
            else:
                self.mensaje = "No de pudo guardar intentelo mas tarde"
                print fondo.errors

        else:
            redirect('login')
        result = self.render_to_response(self.get_context_data())
        return result

    def get_context_data(self, **kwargs):
        context = super(AhorrosPageView, self).get_context_data(**kwargs)
        context['fondo_form'] = self.fondo_form
        context['aportacion_form'] = self.aportacion_form
        context['mensaje'] = self.mensaje
        context['por_aportacion'] = self.por_aportacion

        return context


class RentabilidadPageView(AuthenticatedView):
    template_name = 'core/rentabilidad.html'


class ResultadosPageView(AuthenticatedView):
    template_name = 'core/resultados.html'


def signupView(request):
    if request.method == "POST":
        form = RegistrationForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)

            return redirect('home')

        else:
            return render(request, "core/signup.html", {'form': form})

    else:
        if request.user.is_authenticated():
            return redirect('home')
        else:
            return render(request, "core/signup.html", )

